//
//  hat_21_face_detetctionApp.swift
//  hat-21-face-detetction
//
//  Created by Michaud Reyna on 12/15/21.
//

import SwiftUI

@main
struct hat_21_face_detetctionApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
